The JSON file in this repository encodes where and how Tor technology is
blocked.  We, the anti-censorship team, update the JSON file whenever we learn
about new censorship events.  We chose JSON as encoding format because the
primary consumer of this file is Tor Browser and JSON is particularly easy for
Tor Browser to parse.

JSON fields
-----------

The JSON file has the following format:

```json
{
  "COUNTRY_CODE": {
    "default": {
      "infrastructure": {
        "website": BOOL,
        "bridgedb_www": BOOL,
        "bridgedb_moat": BOOL
      },
      "relays": BOOL,
      "dirauths": BOOL,
      "bridges": {
        "default": BOOL,
        "published_vanilla": BOOL,
        "unpublished_vanilla": BOOL,
        "published_obfs4": BOOL,
        "unpublished_obfs4": BOOL,
        "meek": BOOL
      }
    },
    "ASN": { (optional)
      "infrastructure": {
        "website": BOOL,
        "bridgedb_www": BOOL,
        "bridgedb_moat": BOOL
      },
      "relays": BOOL,
      "dirauths": BOOL,
      "bridges": {
        "default": BOOL,
        "published_vanilla": BOOL,
        "unpublished_vanilla": BOOL,
        "published_obfs4": BOOL,
        "unpublished_obfs4": BOOL,
        "meek": BOOL
      }
    },
    ...
  },
  ...
}
```

* `COUNTRY_CODE`: Two-letter ISO 3166-1 alpha-2 country code (e.g., "cn") which
  specifies the country in question.  Each country code must occur only once.

* `website`: `true` if the website www.torproject.org is reachable over HTTPS
  (i.e., Tor Browser can be downloaded), and `false` if not.

* `bridgedb_www`: `true` if the website bridges.torproject.org is reachable over
  HTTPS and `false` if not.

* `bridgedb_moat`: `true` if the domain-fronted moat interface is reachable and
  `false` if not.

* `relays`: `true` if Tor relays are accessible and `false` if they are not.

* `dirauths`: `true` if at least one directory authority is accessible from the
  location given by `country_code` or `asn`, and `false` if not.

* `default`: `true` if default bridges are accessible and `false` if not.

* `published_vanilla`: `true` if published vanilla bridges (i.e., bridges that
  are distributed over BridgeDB/rdsys) are accessible and `false` if they are
  not.

* `unpublished_vanilla`: `true` if unpublished vanilla bridges (i.e., bridges
  that are *not* distributed over BridgeDB/rdsys) are accessible and `false` if
  they are not.

* `published_obfs4`: `true` if published obfs4 bridges are accessible and
  `false` if they are not.

* `unpublished_obfs4`: `true` if unpublished obfs4 bridges are accessible and
  `false` if they are not.

* `meek`: `true` if meek (over Azure) is accessible and `false` if it is not.

Each country code must contain a `default` key that encodes the default state of
censorship in the given country, i.e. what we believe to be true if we don't
know a user's specific location.  If we know of an autonomous system that has
different blocking behaviour than the country's default section, it should be
encoded in one of the optional `ASN` keys (`ASN` is replaced by the autonomous
system number).  The blocking behaviour in the `ASN` section overrides the
country's `default` section.

Here is a minimal example:

```json
{
  "cn": {
    "default": {
      "infrastructure": {
        "website": false,
        "bridgedb_www": false,
        "bridgedb_moat": true
      },
      "relays": false,
      "dirauths": false,
      "bridges": {
        "default": false,
        "published_vanilla": false,
        "unpublished_vanilla": false,
        "published_obfs4": false,
        "unpublished_obfs4": true,
        "meek": true
      }
    },
    "1234": {
      "infrastructure": {
        "website": true
      }
    }
  }
}
```

Resources
=========

Here are datasets and tools that can tell us if and how Tor is blocked in a
given network:

* Our censorship measurement tool
  [emma](https://gitlab.torproject.org/torproject/anti-censorship/emma).

* [OONI's Explorer](https://explorer.ooni.org).

* [Tor's Metrics Timeline](https://trac.torproject.org/projects/tor/wiki/doc/MetricsTimeline).

* In 2018, the Briar project
  [published a snapshot](https://grobox.de/tor/bridges.html) that documents the
  reachability of bridges, default bridges, and directory authorities from a
  handful of countries.
